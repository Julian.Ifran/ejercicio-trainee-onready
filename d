const formatMoney = (amount, decimalCount = 2, thousands = '.') => {
   try {
      let i = parseInt((amount = Math.abs(Number(amount) || 0))).toString();
      let decimalLimited = (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount));
      decimalLimited = (decimalLimited + '').split('.')[1];
      let j = i.length > 3 ? i.length % 3 : 0;
      return (
         '$' +
         (j ? i.substr(0, j) + thousands : '') +
         i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands) +
         ',' +
         decimalLimited
      );
   } catch (e) {
      console.log(e);
   }
};

const cargarVehiculos = () => {
   return [
      new Auto('Ford', 70, 80000, 5),
      new Auto('Chevrolet', 70, 70000, 5),
      new Auto('Toyota', 70, 75000, 5),
      new Auto('Autosobaconcha', 70, 454440, 5),
   ];
};

function Vehiculo(marca, modelo, Precio) {
   this.marca = marca;
   this.modelo = modelo;
   this.Precio = Precio;
   this.toString = () => {
      return 'marca:' + marca + 'modelo: ' + modelo + 'Precio: ' + Precio;
   };
   this.getFormatedPrice = () => {
      return formatMoney(this.Precio);
   };
}
function Moto(marca, modelo, Precio, Cilindrada) {
   Vehiculo.call(this, marca, modelo, Precio);
   this.Cilindrada = Cilindrada;
   this.toString = () => {
      return (
         'Marca: ' + marca + ' // Modelo: ' + modelo + 'Cilindrada' + Cilindrada + 'Precio: ' + Precio
      );
   };
}
function Auto(marca, modelo, Precio, puertas) {
   Vehiculo.call(this, marca, modelo, Precio);
   this.puertas = puertas;
   this.toString = () => {
      return (
         'Marca: ' +
         marca +
         ' // Modelo: ' +
         modelo +
         ' // Puertas: ' +
         puertas +
         ' Precio: ' +
         this.getFormatedPrice()
      );
   };
}
function imprimirVehiculos(arrayVehiculos) {
   arrayVehiculos.forEach((vehiculo) => {
      console.log(vehiculo.toString());
   });
}
function Concesionaria() {
   this.listaDeVehiculos = cargarVehiculos() || [];
   this.emitirReporte = () => {
      listaDeVehiculos = this.listaDeVehiculos;
      imprimirVehiculos(listaDeVehiculos);
      let automascaro = this.masCaro(listaDeVehiculos);
      let automasbarato = masBarato(listaDeVehiculos);
      console.log('El automascaro', automascaro.toString());
      console.log('El automasbarato', automasbarato.toString());
      // encontrarLaLetra("Y",concesionaria)
      console.log('=============================');
      ordenarPorPrecio(listaDeVehiculos);
   };
   this.masCaro=(arrayVehiculos) =>{
      arrayVehiculos.sort((a, b) => (a.Precio < b.Precio ? 1 : -1));
      return arrayVehiculos[0];
   }
}



let concesionaria = new Concesionaria();
concesionaria.emitirReporte();


function masBarato(arrayVehiculos) {
   arrayVehiculos.sort((a, b) => (a.Precio > b.Precio ? 1 : -1));
   return arrayVehiculos[0];
}

function encontrarLaLetra(letra, arrayVehiculos) {
   let resultado;
   letra = letra.toUpperCase();
   arrayVehiculos.forEach(function (producto) {
      if (producto.Modelo.includes(letra)) {
         resultado = arrayVehiculos.filter((producto) => producto.Modelo.includes(letra));
         console.log(
            `Vehículo que contiene en el modelo la letra ${letra.toUpperCase()}: ${producto.Marca} ${
               producto.Modelo
            } $${producto.Precio}`,
         );
      }
   });
   if (resultado == undefined) {
      console.log(`Ningún Vehículo contiene la letra: ${letra.toUpperCase()}`);
   }
}

function ordenarPorPrecio(arrayVehiculos) {
   console.log('Vehículos ordenados por Precio de mayor a menor');
   arrayVehiculos.sort((a, b) => a.Precio - b.Precio);
   arrayVehiculos.forEach((producto) => {
      console.log(`${producto.Marca}  ${producto.Modelo} `);
   });
}